module.exports={
    title:'Coding Titan',
    author:'Madhusudhan R',
    tagline:'Front End Development',
    social:{
        website:'https://www.codingtitan.com',
        github:'https://www.github.com/itsmadhusudhan',
        twitter:'https://www.twitter.com/itsmadhusudhan',
        linkedin:'https://wwww.linkedin.com/in/itsmadhusudhan'
    }
}
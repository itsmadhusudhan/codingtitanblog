const path = require('path');

//create pages
exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions;

  return new Promise((resolve, reject) => {
    resolve(
      graphql(`
        {
          allMarkdownRemark(
            filter: {
              frontmatter: { draft: { ne: true } }
              fileAbsolutePath: {regex: "/[0-9]+.*--/"}
            }
            limit: 1000
          ) {
            edges {
              node {
                frontmatter {
                  title
                  templateKey
                  categories
                  path
                  date
                }
              }
            }
          }
        }
      `
      ).then(result => {
        if (result.errors) reject(result.errors)

        const { edges } = result.data.allMarkdownRemark
        edges.forEach((edge,index )=> {
          const next=index===0?undefined:edges[index-1].node;
          const prev=index===edges.length-1?undefined:edges[index+1].node;
          createPage({
            path: edge.node.frontmatter.path,
            component: path.resolve(
              `src/templates/${String(edge.node.frontmatter.templateKey)}.jsx`
            ),
            context: {
              prev,
              next
            },
          })
        })
      })
    )
  })
}

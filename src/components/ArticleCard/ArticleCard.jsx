/* eslint-disable */
import React from "react";
import PropTypes from "prop-types";
import { Link } from "gatsby";
import styled from "styled-components";
import readingTime from "reading-time";
import {User,Clock,BookOpen } from 'react-feather';
import react from "../../assets/html5-boilerplate.svg";
import { colors, bgcolors } from "../../styles/styles.js";

const Container = styled(Link)`
  display: block;
  margin: 2em 0em 0.6em 0em;
  cursor: pointer;
  position: relative;
  border-radius: 8px;
  // box-shadow: 1px 2px 20px rgba(0, 0, 0, 0.2);
`;

const HeaderImage = styled.div`
  height: 40%;
  // background:var(--pinkish);
  padding: 5px;
  text-align: center;
  color: var(--white);
  background: ${props => `${bgcolors[props.theme]}`};
  background-image: linear-gradient(to right bottom, #ffb900, #ff7730);
  background-image: linear-gradient(to right bottom, #7ed56f, #28b485);
  background-image: linear-gradient(to right bottom, #2998ff, #5643fa); 

}  
`;

const Content = styled.div`
  min-height: 50%;
  padding: 10px;
  color: ${props => `${colors[props.theme]}`};
`;


const ArticleTitle = styled.h2`
  font-size: 24px;
  font-weight: 400;
  line-height: 1.5;
  text-shadow: 1px 2px 5px rgba(0, 0, 0, 0.25);
  letter-spacing:1px;
`;

const Excerpt = styled.p`
  line-height: 1.5;
  font-size: 18px;
  padding: 10px 10px 20px 10px;
  display: inline-block;
`;

const ReadMore = styled(Link)`
  display: block;
  padding: 0.6em 0.8em;
  background: #fff;
  color: var(--green);
  border-radius: 20px;
  position: absolute;
  bottom: -18px;
  left: 35%;
  box-shadow: 1px 2px 15px rgba(0, 0, 0, 0.2);
  color: #4a4a4a;
`;

// const MetaInfo=styled.div`
// display:flex;
// font-size: 16px;
// padding:10px;
// text-shadow:1px 2px 15px rgba(0,0,0,0.25);  
// align-items:center;
// `;

const MetaInfo = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding-top: 15px;
  flex-wrap:wrap;
  @media screen and (max-width: 500px) {
    width: 100%;
    justify-content: space-between;
  }
  @media screen and (max-width: 450px) {
    flex-wrap:wrap;
    justify-content:center;
  }
  span {
    margin: 0 5px;
  }
`;

const Meta = styled.div`
  display:flex;
  align-items:center;
  justify-content:space-between;
  padding: 0 10px;
  font-size: 15px;
  &:last-child{
    text-align:center;
  }

  @media screen and (min-width:1160px){
    &:last-child{
    margin-top:16px;      
    }
  }
  @media screen and (min-width:350px) and (max-width:972px){
    &:not(first-child){
    margin-top:16px;      
    }
  }
`;

const ReadTime = styled.p`
  font-size: 16px;
  text-align: center;
   text-shadow:1px 2px 15px rgba(0,0,0,0.25);
`;

const ArticleCard = ({
  post: {
    node: {
      frontmatter: { title, path, author, date, theme },
      excerpt,
      html
    }
  }
}) => {
  const textWithoutHTML = html.replace(/<[^>]*>/g, "");
  const readTime = readingTime(textWithoutHTML);
  return (
    <Container to={path}>
      <HeaderImage theme={theme}>
        <ArticleTitle>{title}</ArticleTitle>
      {/*  <MetaInfo>
            <Meta><User/><span>{author}</span></Meta>
            <Meta><Clock/><span> {date}</span></Meta>
            <Meta><BookOpen/><span>{readTime.text}</span></Meta>
          </MetaInfo>
      */}
      </HeaderImage>
      <Content theme={theme}>
        <Excerpt>{excerpt}</Excerpt>
      </Content>
    </Container>
  );
};

export default ArticleCard;

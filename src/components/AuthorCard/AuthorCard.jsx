/* eslint-disable */
import React from "react";
import { Link } from "gatsby";
import styled from "styled-components";
import author from "../../assets/madhu.jpg";
import { Twitter, Github, Linkedin } from "react-feather";

const Container = styled.div`
  padding:20px 30px 30px 30px;
  box-shadow: 1px 2px 20px rgba(0, 0, 0, 0.2);
  margin-bottom: 50px;
  margin-top:50px;
  min-height: 200px;
  color: var(--bluish);
  border-radius: 5px;
  border-top:5px solid var(--green);
`;

const AuthorImage = styled.img`
  display: block;
  float: left;
  width: 110px;
  height: 100px;
  border-radius: 50%;
  margin-right: 2rem;
  margin-bottom: 0.5rem;
  cursor:pointer;
  box-shadow: 1px 2px 20px rgba(0, 0, 0, 0.2);
`;

const Title = styled.h3`
  font-size: 24px;
`;

const Intro = styled.p`
  padding-top: 10px;
`;

const SocialLink = styled.a`
  margin-right:20px;
`;

const PageLink=SocialLink.withComponent(Link).extend`
  margin-right:0;
`

const AuthorCard = props => {
  return (
    <Container>
      <AuthorImage src={author} />
     <PageLink to="/about"><Title>Madhusudhan R</Title></PageLink>
   {/*   <Intro>
        My name is Madhusudhan, I'm a self taught front-end developer. I love
        building user interfaces that actually solves problems. Apart from
        coding I love to read books.
      </Intro>
      <Intro>You can know more about me on <PageLink to="/about">About page</PageLink></Intro>
   */ }
   <Intro>Front End Developer</Intro>
      <Intro>
        <SocialLink href="https://twitter.com/itsmadhusudhan" target="__blank">
          <Twitter/>
        </SocialLink>
        <SocialLink href="https://github.com/itsmadhusudhan" target="__blank">
          <Github/>
        </SocialLink>
        <SocialLink href="https://linkedin.com/in/itsmadhusudhan" target="__blank">
          <Linkedin/>
        </SocialLink>
      </Intro>
    </Container>
  );
};

export default AuthorCard;

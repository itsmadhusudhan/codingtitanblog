/* eslint-disable */
import React from "react";
import styled from "styled-components";
import { Link } from "gatsby";
import { bgcolors } from "../../styles/styles.js";
import { Twitter, Github, User } from "react-feather";

const Container = styled.footer`
  display: flex;
  align-items: center;
  padding-left: 50px;
  flex-wrap:wrap;
  padding: 15px;
  justify-content: center;
  background: ${props => `${bgcolors[props.theme]}`};
`;

const Navigation = styled.ul`
  display: flex;
  align-items: center;
  list-style: none;
`;

const FooterLink = styled.a`
  display: flex;
  align-items: center;
  padding-right: 20px;
  color: #fff;
  color: var(--white);
`;

const AboutLink = styled(Link)`
  display: flex;
  align-items: center;
  padding-right: 20px;
  color: #fff;
  color: var(--white);
`;

const Footer = ({ theme }) => {
  return (
    <Container theme={theme}>
      <Navigation>
        <AboutLink to="/about">
          <User />
          <span>About</span>
        </AboutLink>
        <FooterLink href="https://twitter.com/codingtitan" target="__blank">
          <Twitter />
          <span>Twitter</span>
        </FooterLink>
        <FooterLink href="https://github.com/thecodingtitan" target="__blank">
          <Github />
          <span>Github</span>
        </FooterLink>
      </Navigation>
    </Container>
  );
};

export default Footer;

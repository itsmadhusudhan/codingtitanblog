/* eslint-disable */
import React from "react";
import styled from "styled-components";
import { Link } from "gatsby";
import { default as HeaderRoom } from "react-headroom";
import {bgcolors } from "../../styles/styles.js";
import logo from "../../assets/codingtitan-icon.svg";

const Container = styled.header`
  position:relative;
  font-family: "Fira Sans";
  display: flex;
  align-items: center;
  padding: 10px 10px 10px 30px;
  justify-content: center;
  overflow:hidden;
  background: ${props=>`${bgcolors[props.theme]}`};      
`;

const Title = styled.h1`
  font-size: 30px;
  // flex-grow: 1;
`;

const LogoLink = styled(Link)`
  display:flex;
  color:var(--white);
  align-items:center;
`;

const Navigation = styled.ul`
  display: flex;
  align-items: center;
  list-style: none;
  @media screen and (max-width:600px){
    display:none;
  }
`;

export const NavItem = styled(Link)`
  margin-right: 24px;
  text-decoration: none;
  color: var(--bluish);
  color:var(--white);  
`;

const Logo=styled.img`
height:30px;
`

const headerLinks = ["Javascript", "Reactjs", "CSS3", "Typescript", "Sass"];

const Header = ({ theme }) => {
  return (
    <HeaderRoom>
      <Container theme={theme}>
        <Title>
          <LogoLink to="/"><Logo src={logo}/>Coding Titan</LogoLink>
        </Title>
        {/*
      <Navigation>
        {headerLinks.map(link => {
          return <NavItem key={link} to={`/${link.toLocaleLowerCase()}`}>{link}</NavItem>;
        })}
      </Navigation>
      */ }
      </Container>
    </HeaderRoom>
  );
};

export default Header;

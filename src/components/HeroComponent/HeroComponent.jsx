/* eslint-disable */
import React from "react";
import styled from "styled-components";
import logo from "../../assets/icon.svg";

const Hero = styled.div`
  font-family: "Fira Sans"
  padding: 80px 0 60px 0;
  color:#072642;
  color: var(--bluish);
  overflow: hidden;
  position: relative;
`;

const HeroContent = styled.div`
  padding: 45px 10px;
  text-align: center;
  @media screen and (max-width: 972px) {
    position: relative;
    right: 0;
  }
`;
const ContentHeading = styled.h2`
  font-size: 36px;
  line-height: 1.7;
  text-shadow: 1px 2px 2px rgba(0, 0, 0, 0.2);
`;

const ContentBody = styled.p`
  font-size: 20px;
`;

const Logo=styled.img`
  height:10em;
  user-select:none;
  padding-left:30px;
`

const HeroComponent = () => {
  return (
    <Hero>
      <HeroContent>
      <Logo src={logo}/>
        <ContentHeading>Welcome to Coding Titan Blog</ContentHeading>
        <ContentBody>
          Learn the most exciting and cool stuff in Front End Development along with me
        </ContentBody>
      </HeroContent>
    </Hero>
  );
};

export default HeroComponent;

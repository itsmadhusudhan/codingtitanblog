/* eslint-disable */
import React, { Fragment } from "react";
import { injectGlobal } from "styled-components";
import Helmet from "react-helmet";
import Header from "../Header";
import Footer from "../Footer";
import favicon from "../../assets/favicon.png";
import "../../styles/font.css";
import "prismjs/themes/prism-tomorrow.css";

injectGlobal`
  :root{
    --green:#28b485;
    --black:#333;
    --white:#fff;
    --violet:#a45b96;
    --purple:rgba(150,71,190,1);
    --yellow:#ffb648;
    --greenish:linear-gradient(to top right,#73BDC6,#9CD9DE);
    --pinkish:#FB5762;
    --bluish:#072642;
    --lightblue:#BDECFF;
    --red:#EF432B;
    --blue:#0FB9C9;
  }

  html{
    height:100%;
    box-sizing:border-box;
  }

  *,*::before,*::after{
    box-sizing:inherit;
  }

  body{
    margin:0;
    font-family:'Fira Sans',Oxygen,'Open Sans',sans-serif,-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto,
    Ubuntu, Cantarell,'Helvetica Neue';
    font-size:1rem;
    color:#333;
    font-weight:400;
  }

  a{
    text-decoration:none;
    color:inherit;
  }

  h1,h2,h3,h4,h5,h6,ul,li,p{
    margin:0;
    padding:0;
  }
`;


const Layout = ({ children }) => {
  return (
    <Fragment>
      <Helmet title="Coding Titan | Learn Front End Development along with me">
        <html lang="en" />
        <link rel="icon" type="image/png" sizes="16x16" href={favicon} />
        <meta name="theme-color" content="#072642" />
        <meta
          name="description"
          content={`
       A blog for programmers by another programmer. I'm Madhusudhan. A passionate programmer.
      `}
        />
      </Helmet>
      <Header theme="bluish" />
      {children}
      <Footer theme="bluish" />
    </Fragment>
  );
};

export default Layout;

/* eslint-disable */
import React from "react";
import PropTypes from "prop-types";
import { Link } from "gatsby";
import styled from "styled-components";
import readingTime from "reading-time";
import { User, Clock, BookOpen } from "react-feather";
import { colors, bgcolors } from "../../styles/styles.js";
import hero from "../../pages/articles/2018-08-12--different-ways-of-styling-html-tags-with-css/ordinaryday.svg";

const Container = styled(Link)`
  display: flex;
  flex-direction: column;
  margin: 2.5em 1em 0.6em 1em;
  cursor: pointer;
  position: relative;
  border-radius: 8px;
  // box-shadow: 1px 2px 20px rgba(0, 0, 0, 0.2);
  // border:1px solid #ccc;
  @media screen and (max-width: 786px) {
    margin: 20px;
  }
`;

const HeaderImage = styled.img`
  display:block;
  width:100%;
  text-align: center;
}  
`;

const Content = styled.div`
  padding: 10px 10px 10px 10px;
  color: ${props => `${colors[props.theme]}`};
`;

const ArticleTitle = styled.h2`
  font-size: 24px;
  font-weight: 400;
  line-height: 1.2;
  // text-shadow: 1px 2px 5px rgba(0, 0, 0, 0.25);
  letter-spacing: 1px;
`;

const Excerpt = styled.p`
  line-height: 1.5;
  font-size: 18px;
  padding: 10px 10px 0px 0px;
  display: inline-block;
`;

// const MetaInfo=styled.div`
// display:flex;
// font-size: 16px;
// padding:10px;
// text-shadow:1px 2px 15px rgba(0,0,0,0.25);
// align-items:center;
// `;

const MetaInfo = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding:10px;
  flex-wrap: wrap;
  @media screen and (max-width: 500px) {
    width: 100%;
    justify-content: space-between;
  }
  @media screen and (max-width: 450px) {
    flex-wrap: wrap;
    justify-content: center;
  }
  span {
    margin: 0 5px;
  }
`;

const Meta = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 5px 10px;
  font-size: 15px;
  &:last-child {
    text-align: center;
  }
`;

const ReadTime = styled.p`
  font-size: 16px;
  text-align: center;
  text-shadow: 1px 2px 15px rgba(0, 0, 0, 0.25);
`;

const NewArticleCard = ({
  post: {
    node: {
      frontmatter: { title, path, author, date, theme },
      excerpt,
      html
    }
  }
}) => {
  const textWithoutHTML = html.replace(/<[^>]*>/g, "");
  const readTime = readingTime(textWithoutHTML);
  return (
    <Container to={path}>
      <HeaderImage src={hero} />
      <MetaInfo>
        <Meta>
          <User />
          <span>{author}</span>
        </Meta>
        <Meta>
          <Clock />
          <span> {date}</span>
        </Meta>
        <Meta>
          <BookOpen />
          <span>{readTime.text}</span>
        </Meta>
      </MetaInfo>
      <Content theme={theme}>
        <ArticleTitle>{title}</ArticleTitle>
        <Excerpt>{excerpt}</Excerpt>
      </Content>
    </Container>
  );
};

export default NewArticleCard;

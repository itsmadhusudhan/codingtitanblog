import React from "react";
import styled from "styled-components";

const Wrapper = styled.div`
  padding: 50px;
  display:flex;
  flex-direction:column;
  text-align:center;
`;

const Form = styled.form`
  display:flex;
  flex-direction:column;
  align-items:center;
  align-content:center;
  text-align: center; 
  margin-top:1.5em; 
`;

const Title=styled.h3`
  margin-bottom: 15px;  
  font-weight: 200;
  color: var(--bluish);
  font-size: 1.5rem;
`

const UserInput = styled.input`
  margin: 0.6em 0;
  padding: 10px;
  border-radius: 5px;
  box-shadow: 1px 2px 15px rgba(0, 0, 0, 0.25);
  outline: none;
  width: 290px;
  text-align: center;
  border-width: 2px;
  border-color: var(--green);
  border-style: solid;
  transition: border 1s ease;
  display:block;
  &:focus {
    border-color: rgba(0, 0, 0, 0.1);
    outline: 0;
    border-style: solid;
  }
`;

const UserInputSubmit = UserInput.extend`
  margin: 0.8em 0;
  padding: 9px;
  border-radius: 5px;
  border: 0;
  box-shadow: 1px 2px 15px rgba(0, 0, 0, 0.25);
  outline: none;
  width: 140px;
  cursor: pointer;
  background: #10c19c;
  color: #fff;
  font-size: 18px;
  &:hover {
    box-shadow: 1px 2px 25px rgba(0, 0, 0, 0.25);
    transform: scale(1.01);
  }
`;

const NewsLetter = (props) => {
  return (
    <Wrapper>
    <Title>{props.title}</Title>
      <Form
        action="https://www.getrevue.co/profile/madhusudhan/add_subscriber"
        method="post"
        id="revue-form"
        name="revue-form"
        target="_blank"
      >
        <UserInput
          placeholder="Your Email..."
          type="email"
          name="member[email]"
          id="member_email"
        />
      {/*  <UserInput
          placeholder="Your Name..."
          type="text"
          name="member[first_name]"
          id="member_first_name"
  />*/}
          <UserInputSubmit
            type="submit"
            value="Yes, please"
            name="member[subscribe]"
            id="member_submit"
          />
      </Form>
    </Wrapper>
  );
};

export default NewsLetter;

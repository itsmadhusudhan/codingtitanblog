import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Link } from "gatsby";
import { ArrowLeft, ArrowRight } from "react-feather";

const PageLinkBar = styled.div`
  // display: flex;
  padding: 30px 0;
  font-size: 24px;
  // justify-content: space-between;
  align-items: center;
`;

const LinkPage = styled(Link)`
  display: flex;
  align-items: center;
  float:left;
`;

const PageLink = ({ prev, next }) => {
  return (
    <PageLinkBar>
      {prev && (
        <LinkPage to={prev.frontmatter.path}>
          <ArrowLeft />
          <span>Previous post</span>
        </LinkPage>
      )}
      {next && (
        <LinkPage to={next.frontmatter.path} style={{"float":"right"}}>
          <span>Next post</span>
          <ArrowRight />
        </LinkPage>
      )}
    </PageLinkBar>
  );
};

PageLink.propTypes = {
  prev: PropTypes.object,
  next: PropTypes.object
};

export default PageLink;

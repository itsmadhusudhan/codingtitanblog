/* eslint-disable */
import React from "react";
import PropTypes from "prop-types";
import { Link } from "gatsby";
import styled from "styled-components";
import readingTime from "reading-time";
import { postCovers } from "../../styles/styles";
import { User, Clock, BookOpen } from "react-feather";

const randomColor = function() {
  let color = "";
  for (var i = 0; i < 6; i++) {
    color = postCovers[Math.floor(Math.random() * 6)];
  }
  return color;
};

const Container = styled(Link)`
  min-height: 300px;
  position: relative;
  box-shadow: 0px 1px 20px 0px rgba(0, 0, 0, 0.15);
  cursor: pointer;
  margin: 10px;
`;

const PostCover = styled.div`
  overflow: hidden;
  height: 200px;
  width: 100%;
  color: var(--white);
  img {
    width: 120%;
  }
  &:hover {
  }
`;

const Category=styled.div`
position: absolute;
top: -37px;
left: 0;
background: var(--bluish);
padding: 10px 15px;
color: #FFFFFF;
font-size: 14px;
font-weight: 600;
text-transform: uppercase;
`

const Date = styled.div`
  position: absolute;
  top: 10px;
  right: 20px;
  background: var(--bluish);
  width: 65px;
  height: 65px;
  padding: 8px 0;
  border-radius: 100%;
  color: #fff;
  font-weight: 700;
  text-align: center;
  font-size: 14px;
  user-select: none;
`;

const PostContent = styled.div`
  padding: 15px;
  position:relative;
`;

const PostTitle = styled.h2`
  padding: 10px;
  padding-top: 80px;
`;

const PostExcerpt = styled.p`
  padding-bottom: 20px;
`;

const PostMeta = styled.div`
  display: flex;
`;

const Info = styled.p`
  padding-right: 20px;
  display:flex;
  align-items:center;
  span{
    padding-left:3px;
  }
`;

const PostCard = ({
  post: {
    node: {
      frontmatter: { title, categories, path, author, date, theme },
      excerpt,
      html
    }
  }
}) => {
  const textWithoutHTML = html.replace(/<[^>]*>/g, "");
  const readTime = readingTime(textWithoutHTML);
  return (
    <Container to={path}>
      <PostCover style={{background: randomColor()}}>
        <Date>{date}</Date>
        <PostTitle>{title}</PostTitle>
      </PostCover>
      <PostContent>
      <Category>{categories[0]}</Category>
        <PostExcerpt>{excerpt}</PostExcerpt>
        <PostMeta>
          <Info><User />
          <span>{author}</span></Info>
          <Info><BookOpen />
          <span>{readTime.text}</span></Info>
        </PostMeta>
      </PostContent>
    </Container>
  );
};

export default PostCard;

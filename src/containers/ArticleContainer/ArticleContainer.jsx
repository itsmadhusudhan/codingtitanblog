/* eslint-disable */
import React, { Fragment } from "react";
import PropTypes from "prop-types";
import NewArticleCard from "../../components/NewArticleCard";
import PostCard from "../../components/PostCard";
import styled from "styled-components";

const Container = styled.section`
  display: grid;
  grid-template-columns: repeat(3,minmax(min-content, 1fr));
  @media screen and (max-width: 1050px) {
    grid-template-columns: repeat(2,minmax(min-content, 1fr));  
    }
  @media screen and (max-width: 686px) {
    grid-template-columns: minmax(min-content, 1fr);
  }
 
`;

const ArticleContainer = ({ posts }) => {
  return (
    <Fragment>
      <Container>
        {posts &&
          posts.map(post => {
            return <PostCard key={post.node.frontmatter.path} post={post} />;
          })}
      </Container>
    </Fragment>
  );
};

export default ArticleContainer;

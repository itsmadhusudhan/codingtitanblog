/* eslint-disable */
import React, { Fragment } from "react";
import PropTypes from "prop-types";
import ArticleCard from "../../components/ArticleCard";
// import NewArticleCard from "../../components/NewArticleCard";
import styled from "styled-components";

const Container = styled.section`
  padding:10px;
  margin-right:10px;
  // background:var(--green);  
`;

const SideBar=styled.div`
`

const SideBarContainer = ({ posts }) => {
  return (
    <Fragment>
      <Container>
        {posts &&
          posts.map(post => {
            return <ArticleCard key={post.node.frontmatter.path} post={post} />;
          })}
      </Container>
    </Fragment>
  );
};

export default SideBarContainer;

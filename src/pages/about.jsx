import React from "react";
import styled from "styled-components";
import Layout from "../components/Layout";

const Container = styled.div`
  min-height: 100vh;
  max-width: 800px;
  margin: 0 auto;
  font-size: 21px;
  line-height: 1.5em;
  width: 100%;
  padding: 50px 15px 100px 30px;
  p {
    line-height: 1.6;
    color: #1a1a1a;
    margin-bottom: 10px;
  }
  h1 + *,
  h2 + *,
  h3 + *,
  h4 + *,
  h5 + *,
  h6 + * {
    margin-top: 20px;
  }
`;

const Heading = styled.h1`
  font-size: 28px;
  text-align: center;
`;

const SocialLink = styled.a`
  color: var(--green);
  font-weight: bold;
`;

const AboutPage = () => {
  return (
    <Layout>
      <Container>
        <Heading>About Me</Heading>
        <p>
          Hello All, I'm Madhusudhan. I'm a self taught Front-End Web developer.{" "}
        </p>
        <p>
          I have started Coding Titan for several reasons. The most important
          among them is helping others while I learn.
        </p>
        <p>
          I have planned to consistently blog about the latest things in
          Front-End world. All these time one thing I have learnt is that for
          anything basics should be strong. Frameworks and libraries might come
          and go but the language basics are here to stay!
        </p>
        <p>
          This blog will evolve with time. I will add more features and quality
          content.
        </p>
        <p>
          You can find me on{" "}
          <SocialLink href="https://twitter.com/itsmadhusudhan" target="__blank"> Twitter </SocialLink>,
          <SocialLink href="https://github.com/itsmadhusudhan" target="__blank"> Github </SocialLink>or
          <SocialLink href="https://medium.com/@itsmadhusudhan" target="__blank"> Medium</SocialLink>
        </p>
      </Container>
    </Layout>
  );
};

export default AboutPage;

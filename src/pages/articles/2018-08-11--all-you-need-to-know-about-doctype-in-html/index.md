---
templateKey: PostTemplate
title: "All you need to know about DOCTYPE in html"
date: "2018-08-11T22:36:29.875Z"
path: "/all-you-need-to-know-about-doctype-in-html"
categories:
  - html
tags: [html, codingnewbie]
description: "Learn all about the why and how about using doctype in html document"
author: Madhusudhan R
featured: true
draft: false
theme: bluish
coverImage: ""
---

If you are getting started to learn writing html, the first line you write is doctype.

We will write something like below in html5.

```html
<!DOCTYPE html>
or
<!doctype html>
```

First we write the doctype and then continue writing other html tags.

Doctype is not case sensitive so you don't have to worry.

## Why write a DOCTYPE?

Doctype is written to tell browsers which version of html we are using.

Currently html is in version 5.

In previous version of html i.e. HTML 4.01 it was written like below.

```html
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">

```

You had to reference dtd document for the doctype in the former versions but now it's just a simple tag.

It is even ok to write html without doctype!

**But you need to write doctype because when browsers encounter the tag they parse html in standard mode.**

## Standard Mode vs Quircks Mode

A quirck mode refers to technique that some browsers uses for the sake of maintaining backward compatibility with web pages designed for internet explorer 5 and earlier versions.

A standard mode strictly complies with W3C standards to render the html.

If you donot write the doctype in modern browsers they render in quircks mode which might not be complied with current standards.

So always write doctype in the beginning of an html document so that it valid.

## Why have two modes?

Well back then when browsers were emerging each one had diffrent algorithms and techniques to render a webpage and they didnot adher to the standards.

This had caused problems. So they standardised on how a browser should render a page.

As an early browser internet explorer had lot many implemtations that's not standard. Even now internet explorer is one of the most used browser so it is necessary to support for webpages designed for its earlier versions.

IE has come a long way but still it is far from what other browsers provide.

We will talk about browsers in a separate article.

I will update this article if I know anything more with time.

If you have liked this article consider sharing it with your network.
---
templateKey: PostTemplate
title: "Different ways of styling HTML tags with css"
date: "2018-08-12T09:15:29.875Z"
path: "/different-ways-of-styling-html-tags-with-css"
categories:
    - html
    - css
tags: [html,css,codingnewbie]
description: "You are going to learn different ways of styling html tags and pros and cons of each method"
author: Madhusudhan R
featured: true
draft: false
theme: bluish
coverImage: ordinaryday.svg
---

Today I'm going to discuss with you the different ways you can style html tags.
HTML is just a bare bones of a webpage. Styling the html tags makes it look nicer and beautiful.

![styling-html](ordinaryday.svg)

After all the HTML has been parsed browser starts painting the tags with css styles.

Styling is applied sequentially. We will talk about this in a while.

Each html tag has certain _properties_ whose _values_ are changed by applying styles with css as necessary.

Now without further ado let's start.

## **1. Inline Styling**

When you have first started learning web development most of the time this is the first way you learn to style.

here's an example

Each html element has its own set of properties which may be unique to them or a common one.

Now consider you have a div element. You want to give it a background color and margin. Here's how you do it.

```html
<div style="background:red;margin:200px;">Background is red</div>
```

In the above example we have included a style attribute on the div element. set the background color to red and changed margin to 200px.
You can add as many properties as you want inline on an html element.

**pros:**

- Styles will be there along with markup.
- Can be added quickly.

**cons:**

- It becomes inefficient and hard to maintain when styling a large html file.
- It leads to code duplication as the inline styles are specific to that element.
- Additionally it is difficult to read.

**My take:**

Well I will avoid this as much as I can as there are better ways to write more modular and reusable styles.

**It is good to know how to write it. Give it a spin and know by yourself how much css you repeat.**

## **2. Using style tags**

There has to be a better approach to it.

Well there is. It's by using style tags.

Before telling about how to use it, let me tell you some things about css. CSS gives us different ways to identify an html element.

You need to first identify an html you want to style right?

You can read more about selecting an html with css in this article.

I will just brief here.

You can select an html element by the tag name itself like div,input,hr,span etc.

By using **ids** which are unique to each html element you have in a page. You use it only once in a page.

By using classes which can be reusable any number of times.

Now here's the example. We will style different html element in 3 different ways I have mentioned above.

```html
<html>
<head>
...
<style type="css/text">
    div{
        background:#ccc;
    }
    #heading{
        color:green;
    }
    .line{
        width:500px;
    }
</style>
</head>
<body>
    <div>
        <h1 id="heading">Using css here</h1>
        <hr class="line"/>
    <div>
</body>
</html>
```

All the div's that is written after this will have the same styles as first div since html element has been selected for stying.

Id heading is only for that first h1 tag and should not be used else where.

class can used anywhere on any element. If that element has that property then the class changes the value.

So now you are clear how to write css using the style tags.

All the properties you use in inline styling are used here.

**pros:**

- Enables to read and write more reusable code.

**cons:**

- It is better than the former inline styling but this method also difficult to manage in a large file.

  **Sometimes its is ok to use this method when you need to quickly style something or have less styling**

## **3. Using External stylesheet**

This method separates the markup and styling from one another.

I'm going to use the same example above but separating the style and markup.

```css
<!--styles.css -- > div {
  background: #ccc;
}
#heading {
  color: green;
}
.line {
  width: 500px;
}
```

Now we have our styling ready how do we include the styles sheet in our markup file?

It is by using link tag in the head section.

```html
<html>
<head>
  ...
  <link href="./styles.css" rel="stylesheet">
</head>
<body>
  <div>
    <h1 id="heading">Using css here</h1>
    <hr class="line"/>
  <div>
</body>
</html>
```

In the example above stylesheet (we call css files as stylesheet) **styles.css** is linked to html file with link tag and observe **rel** attribute is used to specify browser that this is stylesheet/css file.

Without using that attribute browser don't parse and apply the styles to the markup.

**pros:**

- Complete separation of styling with markup
- Enables Modularity and reusability to the core.

**cons:**

- Browser need to download the file first to start parsing the css.

**This is the best available and the most often used approach to write css.**

**_Note:_**

When the browser sees the external stylesheet it ignore the inline styling.
for eg: you have applied color to be red to an html element using inline styling and you have given it green color in the external stylesheet. Now the inline style color red will be ignored.

That's it. In this article we have looked into

- Different ways to style html tags
- pros and cons of each

If you have liked the article please do consider sharing it with your network.

---
templateKey: PostTemplate
title: "Learn how to use css box-sizing property"
date: "2018-08-15T16:10:29.875Z"
path: "/learn-how-to-use-css-box-sizing-property"
categories:
    - css
tags: [css,codingnewbie]
description: "You will be learning why and how to use css box-sizing property"
author: Madhusudhan R
featured: true
draft: false
theme: bluish
coverImage: ""
---

Today we are going to learn the box-sizing property which we all use but still many aren't aware why it is used?

Like any other CSS property `box-sizing` property is also defined inside any class, id or element selector.

Normally browsers calculate the height and width of the elements you define and render them accordingly.

If you look into the CSS box model you can see that an element has content box, border, padding and margin.

Since the margin is the gap/distance between two elements that's not added to the height or width of an element but the other
3 properties adds up to the height or width of the element and the browser should also take care of this.

By default the width and height is added to the content box and any border or padding of the element is added on top of that.

<p class="highlight highlight-success">To control if the padding and border needs to be included in calculating the total width/height of an element, CSS provides us the box-sizing property.</p>

It has two values **content-box** and **border-box**.

## content-box

The content-box gives the default CSS box-sizing behaviour.

The below example renders the element having width 100px and height 100px.Finally the browser renders the element heigth and width added with the size of border and padding after content box.

```css
.block{
    width:100px;
    height:100px;
    border:1px solid black;
    padding:10px;
    box-sizing:content-box;
}
```

<style>.block{
    width:100px;
    height:100px;
    border:1px solid black;
    padding:10px;
    box-sizing:content-box;
    margin-bottom:2rem;
}</style>
<div class="block">content</div>

<p class="highlight highlight-warn">
So you won't have a box with the dimensions which you wanted. Instead it also includes the border and padding size added upon the width/height.
</p>

## border-box
The default behaviour of the box-sizing can be changed by using border-box value.

Now see the example below.

It also has the same dimensions but I'm using border-box. When it is rendered the content is shrinked and border and padding size is included in the height and width which we specified.

We get exact 100px/100px box.

```css
.newblock{
    width:100px;
    height:100px;
    border:1px solid black;
    padding:10px;
    box-sizing:border-box;
}
```

<style>.newblock{
    width:100px;
    height:100px;
    border:1px solid black;
    padding:10px;
    box-sizing:border-box;
    margin-bottom:2rem;
}</style>
<div class="newblock">content</div>

### Best way of Using

Now you know why and how to use box-sizing but what is the best way to use it?

Some browsers style with border-box but it is a best thing that you use the box-sizing property in the beginning of a CSS file. Also include pseudo classes with it.

```css
*,*::before,*::after{
    box-sizing:border-box;
}
```

<p class="highlight highlight-success">
Always define the CSS code above in your stylesheet
</p>

Read more articles below:

1. [All you need to know about DOCTYPE in html](all-you-need-to-know-about-doctype-in-html)
2. [Different ways of styling HTML tags with css](different-ways-of-styling-html-tags-with-css)

If you have enjoyed reading the article and felt it's worth sharing, then please consider sharing with your friends.
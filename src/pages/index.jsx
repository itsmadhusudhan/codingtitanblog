/* eslint-disable */
import React from "react";
import { graphql } from "gatsby";
import PropTypes from "prop-types";
import styled from "styled-components";
import Layout from "../components/Layout";
import HeroComponent from "../components/HeroComponent";
import ArticleContainer from "../containers/ArticleContainer";

const Container = styled.div`
  max-width: 1054px;
  margin: 0 auto;
  padding: 10px 0px 150px 0;
  display: grid;
  grid-template-columns: 1fr;
  @media screen and (max-width: 1050px) {
    max-width:800px;
    }
    @media screen and (max-width: 686px) {
      max-width:500px;  
    }
`;

const Heading = styled.p`
  padding-top: 50px;
  font-size: 24px;
  text-align: center;
  text-decoration: underline;
`;

class Main extends React.Component {
  render() {
    const { data } = this.props;
    const featuredPosts = data
      ? data.posts.edges.filter(post => post.node.frontmatter.featured === true)
      : [];

    return (
      <Layout>
        <HeroComponent />
        <Heading>Articles</Heading>
        <Container>
          <ArticleContainer posts={featuredPosts} />
        </Container>
      </Layout>
    );
  }
}

export default Main;

Main.propTypes = {
  data: PropTypes.object
};

export const pageQuery = graphql`
  query IndexQuery {
    site {
      siteMetadata {
        title
      }
    }
    posts: allMarkdownRemark(
      filter: {
        frontmatter: { draft: { ne: true } }
        fileAbsolutePath: { regex: "//articles/[0-9]+.*--/" }
      }
      sort: { fields: [frontmatter___date], order: DESC }
    ) {
      edges {
        node {
          frontmatter {
            path
            date(formatString: "Do MMMM YYYY")
            title
            templateKey
            featured
            tags
            categories
            description
            author
            theme
          }
          excerpt(pruneLength: 137)
          html
        }
      }
    }
  }
`;

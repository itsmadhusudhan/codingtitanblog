export const bgcolors = {
  // green: "linear-gradient(to top, var(--green), var(--lightgreen))",
  green:"#28b485",
  lightgreen: "#28b485",
  pinkish: "var(--pinkish)",
  violet: "var(--violet)",
  purple:"var(--purple)",
  bluish:"var(--bluish)"
};
export const colors = {
  green: "#28b485",
  lightgreen: "#7ed56f",
  pinkish: "var(--pinkish)",
  violet: "var(--violet)",
  purple:"var(--purple)",
  bluish:"var(--bluish)"  
};

export const postCovers=[
  "#28b485","#7ed56f","#0FB9C9","#EF432B","#ffb648","#FB5762"
]

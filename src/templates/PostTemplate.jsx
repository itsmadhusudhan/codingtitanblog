/* eslint-disable */
import React, { Fragment } from "react";
import { graphql, Link } from "gatsby";
import Helmet from "react-helmet";
import readingTime from "reading-time";
import styled from "styled-components";
import { User, Clock, BookOpen } from "react-feather";
import PageLink from "../components/PageLink";
import Header from "../components/Header";
import favicon from "../assets/favicon.png";
import AuthorCard from "../components/AuthorCard";
import Footer from "../components/Footer";
// import { colors, bgcolors } from "../styles/styles.js";

const Container = styled.div`
  min-height: 100vh;
  p {
    line-height: 1.6;
    color: #1a1a1a;
    margin-bottom: 16px;
  }

  article > p:first-of-type:first-letter {
    float: left;
    font-size: 48px;
    line-height: 1.2;
    font-weight: bold;
    margin-right: 9px;
  }

  h2 + *,
  h3 + *,
  h4 + *,
  h5 + *,
  h6 + * {
    margin-top: 16px;
  }

  h2,
  h3,
  h4,
  h5,
  h6 {
    color: var(--green);
    margin: 39px 0 0 0;
  }

  ul,
  ol {
    margin-left: 1.1em;
    margin: 1em 1.1em;
  }

  p a {
    text-decoration: underline;
  }

  p strong {
    margin: 16px 0;
  }

  img {
    width: 100%;
    border-radius: 4px;
  }

  img + em,
  .gatsby-resp-image-wrapper + em {
    display: block;
    width: 100%;
    font-size: 1.4rem;
    font-style: normal;
    line-height: 2rem;
    text-align: center;
    margin-top: 8px;
  }

  pre {
    overflow: auto;
    padding: 12px;
    font-size: 1.4rem;
    border-radius: 4px;
    code {
      padding: 0;
      word-wrap: wrap;
    }
  }
  /* Inline code */
  code {
    padding: 4px 8px;
    font-size: 16px;
  }

  blockquote {
    padding-left: 8px;
    color: #757575;
    font-style: italic;

    & p {
      background-color: white;
      padding-left: 8px;
    }
  }

  iframe {
    border: none;
  }
  .gatsby-highlight {
    margin: 30px 0;
    position:relative;
  }

  .highlight{
    background:#FFF7D0;
    border-left:8px solid #28b485;
    padding:10px 24px;
    font-size:18px;
  }

  .highlight-success{
    border-left:8px solid #28b485;
  }

  .highlight-warn{
    border-left:8px solid #E7C000;
  }

  .highlight-danger{
    border-left:8px solid #EF432B;
  }

  pre[class*="language-"]:before{
    position:absolute;
    top:0.8em;
    right:1em;
    font-size:0.75rem;
    color:hsla(0,0%,100%,0.4);
  }

  pre[class*="language-html"]:before{
    content:"html"
  }

  pre[class*="language-css"]:before{
    content:"css"
  }
  
  pre[class*="language-js"]:before{
    content:"js"
  }

  pre[class*="language-yaml"]:before{
    content:"yaml"
  }

  pre[class*="language-sh"]:before{
    content:"sh"
  }

  pre[class*="language-md"]:before{
    content:"md"
  }
`;

{
  /*background: ${props => `${bgcolors[props.theme]}`};*/
}

const Wrapper = styled.div`
  font-family: "Fira Sans";
  padding: 50px 10px 30px 20px;
  text-align: center;
  text-shadow: 1px 2px 3px rgba(0, 0, 0, 0.2);
  color: #28b485;
  color: var(--bluish);
  h1 {
    font-weight: 400;
  }
`;

const MetaInfo = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding-top: 20px;
  @media screen and (max-width: 500px) {
    width: 100%;
    justify-content: space-between;
  }
  @media screen and (max-width: 450px) {
    flex-wrap: wrap;
    justify-content: center;
  }
  span {
    margin: 0 5px;
  }
`;

const Meta = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 10px;
  font-size: 15px;
  @media screen and (min-width: 972px) {
    font-size: 19px;
  }
  @media screen and (max-width: 450px) {
    margin: 0 10px;
    &:last-child {
      margin-top: 10px;
    }
  }
`;

const Article = styled.article`
  max-width: 800px;
  margin: 0 auto;
  font-size: 21px;
  line-height: 1.5em;
  width: 100%;
  padding: 10px 18px 100px 18px;
`;

const Title = styled.h1`
  font-size: 2em;
  font-weight: bold;
  margin-bottom: 8px;
  line-height: 3.1rem;
  @media only screen and (min-width: 1280px) {
    font-size: 2.6em;
  }
`;

const PageLinkWrapper = styled.div`
  max-width: 800px;
  margin: 0 auto;
  padding: 0 30px;
`;

const BlogPost = props => {
  const {
    title: siteTitle,
    author: siteAuthor,
    siteUrl,
    description
  } = props.data.site.siteMetadata;
  const {
    frontmatter: { title, date, path, author, dateRaw, theme },
    html,
    excerpt
  } = props.data.markdownRemark;
  const {
    pathContext: { next, prev }
  } = props;

  const textWithoutHTML = html.replace(/<[^>]*>/g, "");
  const readTime = readingTime(textWithoutHTML);
  return (
    <Fragment>
      <Helmet title={` ${siteTitle} | ${title}`}>
        <html lang="en" />
        <link rel="icon" type="image/png" sizes="16x16" href={favicon} />
        <meta name="theme-color" content="#072642" />
        <meta name="og:title" content={title} />
        <meta name="article:author" content={author} />
        <meta name="article:publisher" content={siteAuthor} />
        {/*<meta
          name="article:published_time"
          content={new Date(dateRaw).toISOString()}
      />*/}
        <meta name="description" content={excerpt} />
        <meta name="og:description" content={excerpt} />
        <meta name="twitter:description" content={excerpt} />
        <meta name="og:type" content="article" />
        <meta name="og:url" content={`${siteUrl}${path}`} />
      </Helmet>
      <Header theme={theme} />
      <Container>
        <Wrapper theme={theme}>
          <Title>{title}</Title>
          <MetaInfo>
            <Meta>
              <User />
              <span>{author}</span>
            </Meta>
            <Meta>
              <Clock />
              <span> {date}</span>
            </Meta>
            <Meta>
              <BookOpen />
              <span>{readTime.text}</span>
            </Meta>
          </MetaInfo>
        </Wrapper>
        <Article dangerouslySetInnerHTML={{ __html: html }} />
      </Container>
      <PageLinkWrapper>
        <PageLink prev={prev} next={next} />
        <AuthorCard />
      </PageLinkWrapper>
      <Footer theme={theme} />
    </Fragment>
  );
};

export default BlogPost;

export const pageQuery = graphql`
  query BlogPostByPath($path: String!) {
    site {
      siteMetadata {
        title
        author
      }
    }
    markdownRemark(frontmatter: { path: { eq: $path } }) {
      html
      excerpt(pruneLength: 320)
      frontmatter {
        title
        date(formatString: "Do MMMM YYYY")
        dateRaw: date
        path
        tags
        author
        description
        theme
      }
    }
  }
`;
